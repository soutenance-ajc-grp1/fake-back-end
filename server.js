const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router(require('./db.js')(), {})
const middlewares = jsonServer.defaults({})

server.use(middlewares)

// If someone do a post request on /api/register, return default json content
server.post('/api/register', (req, res) => {
    res.json({
        username: "TOTO",
        password: "azerty123"
    })
})

// Rewrite default route to /api
server.use(jsonServer.rewriter({
    '/api/*': '/$1',
    '/api/filiere-formateur/search\\?libelle=:libelle': '/filiere-formateur?libelle=:libelle',
    '/api/filiere-gestionnaire/search\\?libelle=:libelle': '/filiere-gestionnaire?libelle=:libelle',
}))

// Use default router
server.use(router)
server.listen(8080, () => {
    console.log('JSON Server is running')
})