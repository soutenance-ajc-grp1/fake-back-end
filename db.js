module.exports = function () {
    return {

        // Modules
        'modules-gestionnaire': require('./API/Modules/modules-gestionnaire.json'),
        'modules-formateur': require('./API/Modules/modules-formateur.json'),
        'modules-stagiaire': require('./API/Modules/modules-stagiaires.json'),

        // Filieres
        'filiere-gestionnaire': require('./API/Filiere/filiere-gestionnaire.json'),
        'filiere-formateur': require('./API/Filiere/filiere-formateur.json'),

        // Users
        'utilisateur-gestionnaire': require('./API/User/utilisateur-gestionnaire.json'),
        'utilisateur-formateur': require('./API/User/utilisateur-formateur.json'),
        'utilisateur-stagiaire': require('./API/User/utilisateur-stagiaire.json'),

        // Other
        'formateurs': require('./API/Personnes/list-formateurs.json'),
        'stagiaire': require('./API/Personnes/list-stagiaire.json')
    }
}